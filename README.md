According to the new GDPR all sensitive data must be first secured before starting processing the dataset.
When we have to check large scale databases for personal / sensitive data it takes time , human and computer resources 
So if we can create an automated system that is able to predict this kind of data will be a useful tool to solve this problem 